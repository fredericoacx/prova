<?php

	$arr_1 = array(
		'ES', 'MG', 'RJ', 'SP', 'MT'
	);

	$arr_2 = array(
		'Mato Grosso', 'São Paulo', 'Rio de Janeiro', 'Minas Gerais', 'Espírito Santo'
	);
	$arr_3 = array_combine($arr_1, array_reverse($arr_2));

	foreach ($arr_3 as $key => $value) {
		echo $key .' - '. $value . '<br>';
	}