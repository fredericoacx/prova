<?php
require 'bootstrap.php';

$statement = <<<EOS
    CREATE TABLE IF NOT EXISTS livro (
        id INT NOT NULL AUTO_INCREMENT,
        titulo VARCHAR(100) NOT NULL,
        autor VARCHAR(100) NOT NULL,
        edicao INT DEFAULT NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    INSERT INTO livro
        (id, titulo, autor, edicao)
    VALUES
        (1, 'Livro 1', 'Autor 1', 1),
        (2, 'Livro 2', 'Autor 2', 1),
        (3, 'Livro 3', 'Autor 3', 1),
        (4, 'Livro 4', 'Autor 4', 1);
EOS;

try {
    $createTable = $dbConnection->exec($statement);
    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}
?>