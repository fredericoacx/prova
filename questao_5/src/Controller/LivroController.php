<?php
namespace Src\Controller;

use Src\TableGateways\LivroGateway;

class LivroController {

    private $db;
    private $requestMethod;
    private $livroId;

    private $livroGateway;

    public function __construct($db, $requestMethod, $livroId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->livroId = $livroId;

        $this->livroGateway = new LivroGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->livroId) {
                    $response = $this->getLivro($this->livroId);
                } else {
                    $response = $this->getAllLivros();
                };
                break;
            case 'POST':
                $response = $this->createLivroFromRequest();
                break;
            case 'PUT':
                $response = $this->updateLivroFromRequest($this->livroId);
                break;
            case 'DELETE':
                $response = $this->deleteLivro($this->livroId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllLivros()
    {
        $result = $this->livroGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getLivro($id)
    {
        $result = $this->livroGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createLivroFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateLivro($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->livroGateway->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateLivroFromRequest($id)
    {
        $result = $this->livroGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateLivro($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->livroGateway->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteLivro($id)
    {
        $result = $this->livroGateway->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->livroGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateLivro($input)
    {
        if (! isset($input['titulo'])) {
            return false;
        }
        if (! isset($input['autor'])) {
            return false;
        }
        if (! isset($input['edicao'])) {
            return false;
        }        
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
